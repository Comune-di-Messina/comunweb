version: '3.7'

services:

    php:
        image: registry.gitlab.com/opencontent/opencity/php:latest
        entrypoint: /scripts/docker-entrypoint.sh php-fpm
        restart: unless-stopped
        depends_on:
            - minio
            - postgres
            - solr
            - redis
            - createbuckets
        volumes:
            - dfs:/mnt/efs/cluster-opencity
            - ./conf.d/google_credentials.json:/var/www/html/google_credentials.json
        environment:
            PHP_INI_ENV_memory_limit: '512M'
            PHP_INI_ENV_date.timezone: 'Europe/Rome'
            PHP_FPM_ENV_pm: dynamic
            PHP_FPM_ENV_pm.min_spare_servers: 1
            PHP_FPM_ENV_pm.start_servers: 5
            PHP_FPM_ENV_pm.max_spare_servers: 8
            PHP_FPM_ENV_pm.max_children: 100
            PHP_FPM_ENV_pm.max_requests: 2000
            EZ_ROOT: /var/www/html
            EZ_INSTANCE: opencity
            NO_FORCE_CONTAINER_REFRESH: 1
            RUN_INSTALLER: 'true'
            RUN_INSTALLER_TRASPARENZA: 'true'
            RUN_INSTALLER_NEWSLETTER: 'true'
            RUN_INSTALLER_DEMO: 'true'
            RUN_REINDEX_CONTENT: 'false'

            GOOGLE_CREDENTIAL_JSON_FILE: "/var/www/html/google_credentials.json"
    
            #### Chiavi recaptcha
            RECAPTCHA_PUBLIC: 6LctC7oUAAAAAKHOT1O4ifUBJd-Wedt5F8NsisG7
            RECAPTCHA_PRIVATE: 6LctC7oUAAAAAN7nNSTwtCY3GP4rpq1AvvA6u7B4
            RECAPTCHA_V3_PUBLIC: 6Le5et4ZAAAAAHRY0tdCEZGQsqzthPflql2jC9HQ
            RECAPTCHA_V3_PRIVATE: 6Le5et4ZAAAAAJlztV_CfEvMUki5ccvWeZUJkPoK

            #### per stringhe: EZINI_FILE__SECTION__VARIABLE: VALUE
            #### per array o hash: EZINI_FILE__SECTION__VARIABLE__KEY: VALUE

            #### Impostazioni nome sito e corollari
            EZINI_site__SiteSettings__SiteName: "OpenCity"
            EZINI_site__SiteSettings__SiteURL: "opencity.localtest.me"
            EZINI_openpa__InstanceSettings__NomeAmministrazioneAfferente: "OpenContent"
            EZINI_openpa__InstanceSettings__UrlAmministrazioneAfferente: "https://www.opencontent.it/opencity"
            EZINI_openpa__GeneralSettings__tag_line: "Il sito web per l'ente pubblico"
            EZINI_openpa__GeneralSettings__theme: cagliari

            #### Impostazioni var dir (unica per istanza)
            EZINI_site__FileSettings__VarDir: var/opencity

            #### Impostazioni varnish
            EZINI_site__HTTPHeaderSettings__CustomHeader: 'enabled' #enabled/disabled html varnish cache
            EZINI_site__VarnishSettings__VarnishHostName: 'varnish' #vedi conf.d/varnish/default.vcl.tmpl#L9
            EZINI_site__VarnishSettings__VarnishPort: '6081'

            #### Impostazioni db
            EZINI_site__DatabaseSettings__Server: postgres
            EZINI_site__DatabaseSettings__Port: 5432
            EZINI_site__DatabaseSettings__User: openpa
            EZINI_site__DatabaseSettings__Password: openp4ssword
            EZINI_site__DatabaseSettings__Database: opencity

            #### Impostazioni db cluster (da impostare come db)
            EZINI_file__eZDFSClusteringSettings__DBBackend: eZDFSFileHandlerPostgresqlBackend
            #EZINI_file__eZDFSClusteringSettings__DFSBackend: eZDFSFileHandlerDFSBackend
            EZINI_file__eZDFSClusteringSettings__DFSBackend: OpenPADFSFileHandlerDFSDispatcher
            EZINI_file__eZDFSClusteringSettings__DBHost: postgres
            EZINI_file__eZDFSClusteringSettings__DBPort: 5432
            EZINI_file__eZDFSClusteringSettings__DBUser: openpa
            EZINI_file__eZDFSClusteringSettings__DBPassword: openp4ssword
            EZINI_file__eZDFSClusteringSettings__DBName: opencity
            EZINI_file__eZDFSClusteringSettings__MountPointPath: '/mnt/efs/cluster-opencity'
            #CLUSTER_STORAGE_GATEWAY_PATH: "extension/ezpostgresqlcluster/clustering/dfs/gateway.php"
            CLUSTER_STORAGE_GATEWAY_PATH: "extension/openpa/classes/clustering/gateway.php"

            #### Impostazioni solr
            EZINI_solr__SolrBase__SearchServerURI: 'http://solr:8983/solr/opencity'

            #### Impostazioni mail
            EZINI_site__MailSettings__AdminEmail: 'no-reply@opencontent.it'
            EZINI_site__MailSettings__EmailSender: ''
            EZINI_site__MailSettings__TransportServer: mailhog
            EZINI_site__MailSettings__TransportPort: 1025
            EZINI_site__MailSettings__TransportConnectionType: '' #Mailhog does not accept the STARTTLS command
            EZINI_site__MailSettings__TransportUser: ''
            EZINI_site__MailSettings__TransportPassword: ''
            ## The FQDN, used by extended HELO See part "4.1.1.1  Extended HELLO (EHLO) or HELLO (HELO)" of http://www.faqs.org/rfcs/rfc2821.html
            EZINI_site__MailSettings__SenderHost: localhost
            EZINI_cjw_newsletter__NewsletterMailSettings__SmtpTransportPort: 1025
            EZINI_cjw_newsletter__NewsletterMailSettings__SmtpTransportServer: mailhog
            EZINI_cjw_newsletter__NewsletterMailSettings__SmtpTransportUser: ''
            EZINI_cjw_newsletter__NewsletterMailSettings__SmtpTransportPassword: ''
            EZINI_cjw_newsletter__NewsletterMailSettings__EmailSubjectPrefix: ''
            EZINI_cjw_newsletter__NewsletterMailSettings__EmailSender: ''

            #### Impostazioni avanzate per il cluster con DFSBackend: OpenPADFSFileHandlerDFSDispatcher
            ## Endpoint del server Redis
            EZINI_openpa_cluster__RedisDFSBackendSettings__Endpoint: 'tcp://redis:6379'

            ## Bucket e Regione S3
            #EZINI_openpa_cluster__AWSS3DFSBackendSettings__Region: ''
            EZINI_openpa_cluster__AWSS3DFSBackendSettings__Bucket: 'opencity-bucket'
            # Dominio che serve le immagini pubbliche: se non configurato è s3-<Region>.amazonaws.com
            #EZINI_openpa_cluster__AWSS3DFSBackendSettings__ServerUri: 'localhost:4567'

            ## Se non si utilizza la strategia ec2_instance-profiles (https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_use_switch-role-ec2_instance-profiles.html)
            ## occorre configurare AWS_PROFILE oppure AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY,AWS_SESSION_TOKEN:
            AWS_ACCESS_KEY_ID: AKIAIOSFODNN7EXAMPLE
            AWS_SECRET_ACCESS_KEY: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY

            ## Solo per configurazioni di emulatori di S3
            EZINI_openpa_cluster__AWSS3DFSBackendSettings__Endpoint: 'http://minio:9000'
            EZINI_openpa_cluster__AWSS3DFSBackendSettings__ServerUri: 'minio-opencity.localtest.me'
            EZINI_openpa_cluster__AWSS3DFSBackendSettings__UsePathStyleEndpoint: 'enabled'

    nginx:
        image: registry.gitlab.com/opencontent/opencity/nginx:latest
        restart: unless-stopped
        depends_on:
            - php
        environment:
            EZ_INSTANCE: opencity

    varnish:
        image: registry.gitlab.com/opencontent/opencity/varnish:latest
        restart: unless-stopped
        labels:
            traefik.enable: 'true'
            traefik.http.services.opencity.loadbalancer.server.port: 6081
            traefik.http.routers.opencity-https.rule: Host(`opencity.localtest.me`)
            traefik.http.routers.opencity-https.entrypoints: websecure
            traefik.http.routers.opencity-https.tls: null
            traefik.http.routers.opencity-https.middlewares: 'frame-allow-self, goodheaders'
            traefik.http.routers.opencity-http.rule: Host(`opencity.localtest.me`)
            traefik.http.routers.opencity-http.entrypoints: web
            traefik.http.middlewares.frame-allow-self.headers.customFrameOptionsValue: 'SAMEORIGIN'
        depends_on:
            - nginx
        environment:
            VARNISH_BACKEND_GRACE: 10m
            VARNISH_BACKEND_HOST: nginx
            VARNISH_CACHE_STATIC_FILES: 1 #attivo ma non gestibile dal vcl iniettato
            VARNISH_DEFAULT_TTL: 300s
            VARNISH_ERRORS_GRACE: 60s

    solr:
        image: registry.gitlab.com/opencontent/opencity/solr:latest
        command: '-s /opt/ez'
        restart: unless-stopped
        volumes:
            - 'solrdata:/opt/ez/ezp-default/data'
        environment:
            EZ_INSTANCE: opencity

    postgres:
        image: 'postgres:9.6-alpine'
        restart: unless-stopped
        environment:
            POSTGRES_DB: opencity
            POSTGRES_USER: openpa
            POSTGRES_PASSWORD: openp4ssword
        volumes:
            - 'pgdata:/var/lib/postgresql/data'
        healthcheck:
            test: ["CMD-SHELL", "pg_isready --user openpa --dbname opencity"]
            interval: 10s
            timeout: 5s
            retries: 5

    traefik:
        image: 'traefik:2.0'
        command:
            - '--global.checknewversion=true'
            - '--global.sendanonymoususage=true'
            #- '--log.level=DEBUG'
            - '--api'
            - '--api.dashboard=true'
            - '--accesslog'
            - '--providers.file.directory=/etc/traefik/dynamic_conf'
            - '--providers.docker=true'
            - '--providers.docker.exposedbydefault=true'
            - '--providers.docker.defaultrule=Host(`{{ normalize .Name }}.localtest.me`)'
            - '--entrypoints.web.address=:80'
            - '--entryPoints.web.forwardedHeaders.insecure'
            - '--entrypoints.websecure.address=:443'
        ports:
            - '80:80'
            - '443:443'
        labels:
            traefik.enable: 'true'
            traefik.http.services.traefik-server.loadbalancer.server.port: 8080
            traefik.http.routers.traefik-https.entrypoints: websecure
            traefik.http.routers.traefik-https.service: api@internal
            traefik.http.routers.traefik-https.rule: Host(`traefik.localtest.me`)
            traefik.http.routers.traefik-https.tls: null
            traefik.http.routers.traefik-https.middlewares: goodheaders
            traefik.http.routers.traefik-http.entrypoints: web
            traefik.http.routers.traefik-http.service: api@internal
            traefik.http.routers.traefik-http.rule: Host(`traefik.localtest.me`)
            traefik.http.routers.traefik-http.middlewares: gotossl
            traefik.http.middlewares.goodheaders.headers.frameDeny: 'true'
            traefik.http.middlewares.goodheaders.headers.stsSeconds: 31536000
            traefik.http.middlewares.goodheaders.headers.stsIncludeSubdomains: 'true'
            traefik.http.middlewares.goodheaders.headers.forceStsHeader: 'true'
            traefik.http.middlewares.goodheaders.headers.contentTypeNosniff: 'true'
            traefik.http.middlewares.goodheaders.headers.featurePolicy: vibrate 'self'
            traefik.http.middlewares.goodheaders.headers.referrerPolicy: strict-origin-when-cross-origin
            traefik.http.middlewares.goodheaders.headers.browserXssFilter: 'true'
            traefik.http.middlewares.goodheaders.headers.customresponseheaders.Cache-control: 'no-cache, private'
            traefik.http.middlewares.gotossl.redirectscheme.scheme: https
        volumes:
            - ./conf.d/traefik/certs:/tools/certs
            - ./conf.d/traefik/config.yml:/etc/traefik/dynamic_conf/conf.yml
            - '/var/run/docker.sock:/var/run/docker.sock'

    mailhog:
        image: mailhog/mailhog
        # must be explicit about what port to proxy from traefik, because the service expose two ports
        labels:
            traefik.enable: 'true'
            traefik.http.services.opencity-mailhog.loadbalancer.server.port: 8025
            traefik.http.routers.opencity-mailhog-https.rule: Host(`mailhog.localtest.me`)
            traefik.http.routers.opencity-mailhog-https.entrypoints: websecure
            traefik.http.routers.opencity-mailhog-https.tls: null
            traefik.http.routers.opencity-mailhog-https.middlewares: goodheaders
            traefik.http.routers.opencity-mailhog-http.rule: Host(`mailhog.localtest.me`)
            traefik.http.routers.opencity-mailhog-http.entrypoints: web

    redis:
        image: redis:latest
        volumes:
            - redisdata:/data
        ports:
            - 6379:6379

    minio:
        image: minio/minio
        volumes:
            - miniodata:/data
        command: server /data
        environment:
            MINIO_ACCESS_KEY: AKIAIOSFODNN7EXAMPLE
            MINIO_SECRET_KEY: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
        ports:
            - "9000:9000"
        labels:
            traefik.enable: 'true'
            traefik.http.services.opencity-minio.loadbalancer.server.port: 9000
            traefik.http.routers.opencity-minio-https.rule: Host(`minio-opencity.localtest.me`)
            traefik.http.routers.opencity-minio-https.entrypoints: websecure
            traefik.http.routers.opencity-minio-https.tls: null
            traefik.http.routers.opencity-minio-https.middlewares: goodheaders
            traefik.http.routers.opencity-minio-http.rule: Host(`minio-opencity.localtest.me`)
            traefik.http.routers.opencity-minio-http.entrypoints: web

    createbuckets:
        image: minio/mc
        depends_on:
            - minio
        entrypoint: >
            /bin/sh -c "
            while ! /usr/bin/mc config host add minio http://minio:9000 AKIAIOSFODNN7EXAMPLE wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY; do echo 'Wait minio to startup...' && sleep 0.1; done;
            /usr/bin/mc mb minio/opencity-bucket;
            /usr/bin/mc policy set public minio/opencity-bucket;
            exit 0;
            "  

volumes:
    dfs:
    solrdata:
    pgdata:
    miniodata:
    redisdata:
